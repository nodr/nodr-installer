#!/bin/bash
#

PKG_MANAGER_LIST="apt yum"
CMD_MINIMAL_LIST="sudo wget"

# minimal
for cmd in ${CMD_MINIMAL_LIST}
do
  [ `which ${cmd}` 1>/dev/null 2>&1 ] || \
  for pkg_manager in ${PKG_MANAGER_LIST}
  do
    # prefix uniquie OS
    case ${cmd} in
      "apt")
        dpkg --configure -a
      ;;
    esac

    # fix
    [ `which ${pkg_manager}` 1>/dev/null 2>&1 ] && ${pkg_manager} install ${cmd} -y

    # postfix uniquie OS

  done 
      
done
  


case `basename $0 | cut -d '_' -f 5` in
  "dev")
    # download current
    wget -N https://gitlab.com/nodr/nodr-installer/-/raw/main/nodr_installer_linux_dev_.sh

    # set run mode
    chmod +x ./nodr_installer_linux_dev_.sh

    # run current from root
    sudo ./nodr_installer_linux_dev_.sh
    ;;
  
  * )
    # download current
    wget -N https://gitlab.com/nodr/nodr-installer/-/raw/main/nodr_installer_linux.sh

    # set run mode
    chmod +x ./nodr_installer_linux.sh

    # run current from root
    sudo ./nodr_installer_linux.sh
    ;;
esac