#!/bin/bash
#
# script install NODR
#

# for case uses shell extglobs
shopt -s extglob

LOG_FILE=/tmp/install_nodr.log
DEBUG_MODE=1
MAX_STEP=8
MAX_OS_VERSION=1000000

REGEX_DIGIT='^[0-9]+$'

## ---------------- multiOS_01 --------------- ##
# version pkg
DOCKER_COMPOSE_VERSION=1.29.2
DISTR_SUPPORTED="
  almalinux_8.6
  centos_7
  debian_10
  ubuntu_20.04
"

START_PKG_LIST="bc curl jq net-tools speedtest-cli wget"
START_PKG_LIST_almalinux="nmap-ncat"
START_PKG_LIST_centos="nmap-ncat"
START_PKG_LIST_debian="ncat"
START_PKG_LIST_ubuntu="ncat"

## ---------------- _multiOS_01 -------------- ##

# OS
OS_TYPE=            # OS type: linux|windows|macos
OS_CODE=            # OS code:   10 |  20   |  30     
OS_FSTAB=/etc/fstab

# HW
CPU_MODEL=
CPU_MODEL_NAME=
CPU_FREQUENCY=      # CPU Hz
CPU_CORE=           # number of cores
CPU_ALL_FREQUENCY=  # CPU_FREQUENCY * CPU_CORE Hz
RAM_TOTAL=          # RAM bytes
BANDWIDTH_UPLOAD=100                      # bandwidth upload bites/s
BANDWIDTH_UPLOAD_MAX_LIMIT=1000000000     # bandwidth limit max upload 1 Gbites/s

# NODR
NODR_USER=nodradmin                     # nodr user
NODR_GROUP=nodradmin                    # nodr group
NODR_USER_HOME=/home/${NODR_USER}
NODR_DIR=${NODR_USER_HOME}/nodr         # work dir nodr
NODR_DOCKER_COMPOSE=docker-compose.yml
NODR_CONFIG=nodr.ini

NODR_HDD=           # hdd nodr /dev/sdb
NODR_HDD_SIZE=      # hdd nodr size bytes
NODR_IP=            # ip  nodr for internet connection
NODR_PORT=          # port nodr
NODR_PORT_DEFAULT=9980
NODR_STORAGE_DIR=
NODR_STORAGE_DRIVE=
NODR_MULTISTORE=
NODR_MULTISTORE_SIZE=

# KEY
KEY_TYPE=prime256v1
KEY_PATH=${NODR_USER_HOME}/.ssh
KEY_PUBLIC=public_key.pem
KEY_PRIVATE=private.key

# api
CURL=curl
NCAT=ncat           # test answer OK code 200
SU=su

# select prod | dev
case `basename $0 | cut -d '_' -f 4` in
  "dev")  API_SETUP_URL="https://apidashboard.tlprt.cloud/api/setup_nodr"     ; CPAR=1000 ;

# --------------- block dev ------------------
# -------------- _block dev ------------------

    ;;

  * )     API_SETUP_URL="https://apidashboard.teleport.media/api/setup_nodr"  ; CPAR=1    ;;
esac

# hw server - parameters are determined based on the configuration
CONFIG_ID=0

DATE=`/bin/date +\%Y.\%m.\%d_\%H-\%M`


# color settings
# "\e[31m": Red, "\e[32m": Green "\e[33m": Yellow "\e[34m": Blue "\e[35m": Magenta, "\e[36m": Cyan, "\e[37m": WHITE, 38+: Grey (default)
def="\033[m"    ## default text color
err="\e[31m"    ## error (red)
rem="\e[32m"    ## remarks, comments (green)
end="\e[32m"    ## finishing message (green)
val="\e[1;32m"  ## value (bold green)
inf="\e[36m"    ## information (blue)
key="\e[1;36m"  ## key for choosing (cyan bold)
act="\e[33m"    ## action, prompts (yellow)
fat="\e[1;31m"


# functions:
#   main()      -- switch mode
#   install()   -- install mode
#   update()    -- update mode
#

MSG_INFO_INSTALLER="${inf}Starting the NODR installation procedure. It will configure your server and launch NODR app in several steps: 
${inf}Step 1: OS version compatibility check.${def} Currently only ubuntu 20.04 or higher is supported.
${inf}Step 2: Assigning ECDSA keys to your server.${def} You may use existing keys or generate new.
${inf}Step 3: Binding public key to your account in dashboard at https://dash.nodr.app.${def} You can log in into existing one or create new account.
${inf}Step 4: Collecting hardware information.${def} Your server must meet the minimum requirements of the NODR configuration you'll choose in the dashboard.
${inf}Step 5: Network configuration.${def} You server must have at least one IP address that is accessible from the internet. 
${inf}Step 6: Storage configuration.${def} Ideally OS drive and storage must be separate and a full drive be dedicated to storage. However, you'll have a chance to configure a more complex storage environment.
${inf}Step 7: Matching your hardware configuration to required minimum.
${inf}Step 8: NODR app automated installation.${def} The NODR app will run in a Docker container under nodradmin user with full rights over the storage. 
${inf}You will have an option to terminate procedure on any step and get back to the beginning.
${act}Shall we start?${def}"

MSG_KEYS_NOT_RECOGNIZED="The public key `[ -f ${KEY_PATH}/${KEY_PUBLIC} ] && cat ${KEY_PATH}/${KEY_PUBLIC} | head -1`\n`[ -f ${KEY_PATH}/${KEY_PUBLIC} ] && cat ${KEY_PATH}/${KEY_PUBLIC} | sed -n '2p' | cut -c 1-5`.....\n`[ -f ${KEY_PATH}/${KEY_PUBLIC} ] && cat ${KEY_PATH}/${KEY_PUBLIC} | sed -n '3p' | cut -c 59-62`\n`[ -f ${KEY_PATH}/${KEY_PUBLIC} ] && cat ${KEY_PATH}/${KEY_PUBLIC} | tail -1`  isn't recognized by NODR backend.\nPlease make sure that you've bound it correctly in the NODR dashboard on app.nodr.io and try again.\nIn case is you need support please contact us via support@nodr.app"
MSG_NODR_BACKEND_NOACCESS="Couldn't reach NODR backend. Please try again later"

# ----------------------------------------------------------------------
# -------------------------------- API ---------------------------------

# API - check created NODR on backend with public key
check_public_key ()
{
  API2MSG=`${CURL} --location --request POST "${API_SETUP_URL}/check_public_key" \
    --header 'application/x-www-form-urlencoded' \
    --data-urlencode 'publicKey='"\`cat ${KEY_PATH}/${KEY_PUBLIC}\`"'' 2>/dev/null` ;
    
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. MSG2API check_public_key:" >> ${LOG_FILE} && \
  echo -e "${CURL} --location --request POST \"${API_SETUP_URL}/check_public_key\" \
    --header 'application/x-www-form-urlencoded' \
    --data-urlencode 'publicKey='\"`cat ${KEY_PATH}/${KEY_PUBLIC}`\"'' 2>/dev/null" 1>>${LOG_FILE} 2>&1 ;

  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. API2MSG check_public_key:" >> ${LOG_FILE} && \
  echo -e "${API2MSG}" 1>>${LOG_FILE} 2>&1 ;
  
  # result  
  echo -e -n "${API2MSG}" 2>/dev/null | jq '.data'
}

# API - check configuration NODR on backend
check_config_nodr ()
{ 
  MSG="{\"bandwidth\":\"${BANDWIDTH_UPLOAD_LIMIT}\", \"id\":${CONFIG_ID}, \"ip\":\"${NODR_IP}\", \"log_level\":\"1\", \"name\":\"nodr\", \"os\":\"${OS_CODE}\", \"os_version\":\"`os_id` ${OS_VERSION_ID}\", \"port\":\"${NODR_PORT}\", \"ram\":\"${RAM_TOTAL}\", \"cpu_frequency\":\"${CPU_ALL_FREQUENCY}\", \"status\":0, \"storage\":\"${NODR_MULTISTORE_SIZE}\", \"ip_run\":\"${NODR_RUN_IP}\", \"port_run\":\"${NODR_RUN_PORT}\", \"nodr_segment_directory\":\"${NODR_MULTISTORE}\" }"
  MSG_SIGN="`echo -n ${MSG} | openssl dgst -sha256 -sign ${KEY_PATH}/${KEY_PRIVATE} | base64`" ;

  PUBLIC_KEY_TXT="`cat ${KEY_PATH}/${KEY_PUBLIC}`" ;

  ${CURL} --location --request POST "${API_SETUP_URL}/check_config_nodr" \
  --header 'application/x-www-form-urlencoded' \
  --data-urlencode 'publicKey='"${PUBLIC_KEY_TXT}"'' \
  --data-urlencode 'sign='"${MSG_SIGN}"'' \
  --data-urlencode 'message='"${MSG}"'' 2>/dev/null | jq '.data' ;
  
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. MSG2API check_config_nodr:" >> ${LOG_FILE} && \
  echo -e "
  ${CURL} --location --request POST \"${API_SETUP_URL}/check_config_nodr\" \
  --header 'application/x-www-form-urlencoded' \
  --data-urlencode 'publicKey='\"${PUBLIC_KEY_TXT}\"'' \
  --data-urlencode 'sign='\"${MSG_SIGN}\"'' \
  --data-urlencode 'message='\"${MSG}\"'' 2>/dev/null | jq '.data'  
  " 1>>${LOG_FILE} 2>&1 ;
  
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. API2MSG check_config_nodr:" >> ${LOG_FILE} && \
  ${CURL} --location --request POST "${API_SETUP_URL}/check_config_nodr" \
  --header 'application/x-www-form-urlencoded' \
  --data-urlencode 'publicKey='"${PUBLIC_KEY_TXT}"'' \
  --data-urlencode 'sign='"${MSG_SIGN}"'' \
  --data-urlencode 'message='"${MSG}"'' 1>>${LOG_FILE} 2>&1 ;
  
}

save_config_nodr ()
{
  [ ! -d ${NODR_DIR} ] && mkdir -p ${NODR_DIR} && chown ${NODR_USER}:${NODR_GROUP} ${NODR_DIR}
  echo "# Config NODR build ${DATE}"              >${NODR_DIR}/${NODR_CONFIG}
  echo "CONFIG_ID=${CONFIG_ID}"                   >>${NODR_DIR}/${NODR_CONFIG}
  echo "OS_CODE=${OS_CODE}"                       >>${NODR_DIR}/${NODR_CONFIG}
  echo "OS_VERSION=`os_id` ${OS_VERSION_ID}"      >>${NODR_DIR}/${NODR_CONFIG}
  echo "CPU_ALL_FREQUENCY=${CPU_ALL_FREQUENCY}"   >>${NODR_DIR}/${NODR_CONFIG}
  echo "NODR_RUN_IP=${NODR_RUN_IP}"               >>${NODR_DIR}/${NODR_CONFIG}
  echo "NODR_RUN_PORT=${NODR_RUN_PORT}"           >>${NODR_DIR}/${NODR_CONFIG}
  echo "NODR_IP=${NODR_IP}"                       >>${NODR_DIR}/${NODR_CONFIG}
  echo "NODR_PORT=${NODR_PORT}"                   >>${NODR_DIR}/${NODR_CONFIG}
  echo "NODR_STORAGE_DIR=${NODR_STORAGE_DIR}"     >>${NODR_DIR}/${NODR_CONFIG}
  echo "NODR_STORAGE_DRIVE=${NODR_STORAGE_DRIVE}" >>${NODR_DIR}/${NODR_CONFIG}
}

docker_compose ()
{ 
  PUBLIC_KEY_TXT="`cat ${KEY_PATH}/${KEY_PUBLIC}`" ;

  ${CURL} --location --request POST "${API_SETUP_URL}/docker_compose" \
  --header 'application/x-www-form-urlencoded' \
  --data-urlencode 'publicKey='"${PUBLIC_KEY_TXT}"'' ;
  
  # log MSG2API
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. MSG2API docker_compose: " >> ${LOG_FILE} && \
  echo -e "
  ${CURL} --location --request POST \"${API_SETUP_URL}/docker_compose\"\n
  --header 'application/x-www-form-urlencoded' \n
  --data-urlencode 'publicKey='\"${PUBLIC_KEY_TXT}\"''\"
  " 1>> ${LOG_FILE} 2>&1 ;
  
  # log API2MSG
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. API2MSG docker_compose: " >> ${LOG_FILE}  && \
  ${CURL} --location --request POST "${API_SETUP_URL}/docker_compose" \
  --header 'application/x-www-form-urlencoded' \
  --data-urlencode 'publicKey='"${PUBLIC_KEY_TXT}"'' 1>>${LOG_FILE} 2>&1 ; 
  
}

info_check_config_nodr ()
{
  ### human conver
  case $1 in
    "os")
      HUMAN_UNIT=
      
      AIM_TMP=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.aim' | sed -e 's/"//g'`
      # decode  
      case ${AIM_TMP} in
        10)    AIM="linux"   ;;
        20)    AIM="windows" ;;
        30)    AIM="macos"   ;;
      esac  ;
      
      USED_TMP=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used' | sed -e 's/"//g'`
      # decode  
      case ${USED_TMP} in
        10)    USED="linux"   ;;
        20)    USED="windows" ;;
        30)    USED="macos"   ;;
      esac  ;

      ;;
      
    "cpu_frequency" )
      HUMAN_UNIT=GHz
      
      AIM_TMP=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.aim' | sed -e 's/"//g'`
      # Hz2GHz
      [ ${#AIM_TMP} -gt 0 ] && let "AIM=${AIM_TMP} / ( 1000 * 1000 * 1000)" || AIM='' ;
      
      USED_TMP=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used' | sed -e 's/"//g'`
      # Hz2GHz  
      [ ${#USED_TMP} -gt 0  ] && let "USED=${USED_TMP} / ( 1000 * 1000 * 1000 )" || USED='' ;
      ;;
      
    "ram")
      HUMAN_UNIT=Gb
      
      AIM=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.aim' | sed -e 's/"//g'`
      # bytes2Gb  
      [ ${#AIM} -gt 0 ] && AIM=`volume_human ${AIM}` || AIM='' ;
      
      USED=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used' | sed -e 's/"//g'`
      # bytes2Gb  
      [ ${#USED} -gt 0 ] && let USED=`volume_human ${USED}` || USED='' ;
      ;;
      
    "storage")
      HUMAN_UNIT=Gb ;
      
      AIM=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.aim' | sed -e 's/"//g'` ;
      # bytes2Gb
      [ ${#AIM} -gt 0 ] && AIM=`volume_human ${AIM}` || AIM='' ;
      
      USED=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used' | sed -e 's/"//g'` ;
      # bytes2Gb  
      [ ${#USED} -gt 0 ] && USED=`volume_human ${USED}` || USED='' ;
      ;;
      
    "bandwidth")
      HUMAN_UNIT=Mbit/s
      
      AIM=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.aim' | sed -e 's/"//g'`
      # bitsS2MbitS  
      [ ${#AIM} -gt 0 ] && AIM=`bandwidth_human ${AIM}` ;
      
      USED=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used' | sed -e 's/"//g'`
      # bitsS2MbitS  
      [ ${#USED} -gt 0 ] && USED=`bandwidth_human ${USED}` || USED='' ;

      ;;

    *)
      HUMAN_UNIT=
      
      AIM=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.aim' | sed -e 's/"//g'`
      USED=`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used' | sed -e 's/"//g'`
      ;;
      
  esac 
  
  
  echo -n -e "${inf}  $1 target: ${key}${AIM} ${inf}${HUMAN_UNIT}${def} "
  
  case "`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.result'`" in
  
    "true")
      #echo -e "${inf}, detected: ${key}`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used'`${inf} => true ${def}"
      echo -e "${inf}, detected: ${key}${USED}${inf} ${HUMAN_UNIT} => success ${def}"
      ;;

    "false")
      #echo -e "${inf}, detected: ${key}`echo ${CHECK_CONFIG_NODR} | jq '.check_config_nodr.'$1'.used'`${err} => false ${def}"
      echo -e "${inf}, detected: ${key}${USED}${inf} ${HUMAN_UNIT} => ${err}false ${def}"
      ;;

    *)
      echo -e "${err} check_config_nodr not supported ${key}$1${err} parameter !!! => false ${def}"
      ;;
      
  esac
  
}

check_nodr_ip ()
{
  API2MSG=`${CURL} --location --request POST "${API_SETUP_URL}/check_nodr_ip" \
    --header 'application/x-www-form-urlencoded' \
    --data-urlencode 'publicKey='"\`cat ${KEY_PATH}/${KEY_PUBLIC}\`"'' \
    --data-urlencode 'ip='"${NODR_IP}"'' \
    --data-urlencode 'port='"${NODR_PORT}"'' 2>/dev/null`
    
  # log MSG2API
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. MSG2API check_nodr_ip: " >> ${LOG_FILE} && \
  echo -e "${CURL} --location --request POST "${API_SETUP_URL}/check_nodr_ip" \
    --header 'application/x-www-form-urlencoded' \
    --data-urlencode 'publicKey='"`cat ${KEY_PATH}/${KEY_PUBLIC}`"'' \
    --data-urlencode 'ip='\"${NODR_IP}\"'' \
    --data-urlencode 'port='\"${NODR_PORT}\"'' 2>/dev/null" 1>>${LOG_FILE} 2>&1;

  # log API2MSG
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. API2MSG check_nodr_ip: " >> ${LOG_FILE} && \
  echo -e "${API2MSG}" >> ${LOG_FILE} ;

  echo -n "${API2MSG}" | jq '.data'
  
#  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. API2MSG check_nodr_ip: " >> ${LOG_FILE} && \
#  ${CURL} --location --request POST "${API_SETUP_URL}/check_nodr_ip" \
#    --header 'application/x-www-form-urlencoded' \
#    --data-urlencode 'publicKey='"`cat ${KEY_PATH}/${KEY_PUBLIC}`"'' \
#    --data-urlencode 'ip='"${NODR_IP}"'' \
#    --data-urlencode 'port='"${NODR_PORT}"'' 1>>${LOG_FILE} 2>&1 || \
#  ${CURL} --location --request POST "${API_SETUP_URL}/check_nodr_ip" \
#    --header 'application/x-www-form-urlencoded' \
#    --data-urlencode 'publicKey='"`cat ${KEY_PATH}/${KEY_PUBLIC}`"'' \
#    --data-urlencode 'ip='"${NODR_IP}"'' \
#    --data-urlencode 'port='"${NODR_PORT}"'' 1>/dev/null 2>&1    

#  ${CURL} --location --request POST "${API_SETUP_URL}/check_nodr_ip" \
#    --header 'application/x-www-form-urlencoded' \
#    --data-urlencode 'publicKey='"`cat ${KEY_PATH}/${KEY_PUBLIC}`"'' \
#    --data-urlencode 'ip='"${NODR_IP}"'' \
#    --data-urlencode 'port='"${NODR_PORT}"'' 2>/dev/null | jq '.data'
   
}

set_active_nodr ()
{
  API2MSG=`${CURL} --location --request POST "${API_SETUP_URL}/set_active_nodr" \
    --header 'application/x-www-form-urlencoded' \
    --data-urlencode 'publicKey='"\`cat ${KEY_PATH}/${KEY_PUBLIC}\`"'' 2>/dev/null` ;

  # log MSG2API
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. MSG2API set_active_nodr: " >> ${LOG_FILE} && \
  echo -e "${CURL} --location --request POST \"${API_SETUP_URL}/set_active_nodr\" \
    --header 'application/x-www-form-urlencoded' \
    --data-urlencode 'publicKey='\"`cat ${KEY_PATH}/${KEY_PUBLIC}`\"'' 2>/dev/null" 1>>${LOG_FILE} 2>&1;
  
  # log API2MSG
  [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "\n$DATE - Step ${STEP}. API2MSG set_active_nodr: " >> ${LOG_FILE}  && \
  echo "${API2MSG}" >> ${LOG_FILE} ;
    
  echo -n "${API2MSG}" | jq '.data'
   
}

# ----------------------------------------------------------------------
# --------------------------------- OS ---------------------------------
# for Step 1: OS version compatibility check. 
# Check distribution
os_type ()
{
  uname -o | cut -d '/' -f2
}

os_code ()
{
  case "`os_type`" in
    "Linux")    echo 10 ;;
    "Windows")  echo 20 ;;
    "MacOS")    echo 30 ;;
  esac
}

os_decode ()
{
  case $1 in
    "10")    echo linux ;;
    "20")    echo windows ;;
    "30")    echo macos ;;
  esac
}


os_id ()
{
  cat /etc/os-release | grep -i "^ID=" | cut -d'=' -f2 | sed -e 's/"//g'
}

os_version_id ()
{
  cat /etc/os-release | grep -i "^VERSION_ID=" | cut -d'=' -f2 | sed -e 's/"//g'
}

check_os_version_id_supported ()
{
  for os in ${DISTR_SUPPORTED}
  do
  
    case "`echo ${os} | cut -d'_' -f1`" in
    
      "$1") 
        echo ${os} | cut -d'_' -f2 ;
        ;;
        
    esac
  
  done

}

## ---------------- multiOS_02 --------------- ##
check_os_supported ()
{
  OS_TYPE="`os_type`" ;
  OS_CODE="`os_code`" ;
  OS_VERSION_ID=`os_version_id` ;
  OS_SUPPORTED=0
  OS_VERSION_ID_SUPORTED=${MAX_OS_VERSION} ;
  
  case "`os_id`" in
 
  # AlmaLinux
  "almalinux")
    add_start_pkg_almalinux ;
    OS_VERSION_ID_SUPORTED=`check_os_version_id_supported almalinux`
    [ `echo ${OS_VERSION_ID} | sed -e 's/\.//'` -ge `echo ${OS_VERSION_ID_SUPORTED} | sed -e 's/\.//'` ] && OS_SUPPORTED=1
    ;;

  # Centos
  "centos")
    add_start_pkg_centos ;
    OS_VERSION_ID_SUPORTED=`check_os_version_id_supported centos`
    [ `echo ${OS_VERSION_ID} | sed -e 's/\.//'` -ge `echo ${OS_VERSION_ID_SUPORTED} | sed -e 's/\.//'` ] && OS_SUPPORTED=1
    ;;

  # Debian
  "debian")
    add_start_pkg_debian ;
    OS_VERSION_ID_SUPORTED=`check_os_version_id_supported debian`  
    [ `echo ${OS_VERSION_ID} | sed -e 's/\.//'` -ge `echo ${OS_VERSION_ID_SUPORTED} | sed -e 's/\.//'` ] && OS_SUPPORTED=1 
    ;;

  # Ubuntu
  "ubuntu")
    add_start_pkg_ubuntu ;
    OS_VERSION_ID_SUPORTED=`check_os_version_id_supported ubuntu`  
    [ `echo ${OS_VERSION_ID} | sed -e 's/\.//'` -ge `echo ${OS_VERSION_ID_SUPORTED} | sed -e 's/\.//'` ] && OS_SUPPORTED=1 
    ;;

  esac

  if [ ${OS_SUPPORTED} -gt 0 ]
  then
 
    [ -n "${LOG_FILE}" ] && echo -e "\n${DATE} - Step ${STEP}. OK - Distribution version supported [ `os_id` ${OS_VERSION_ID} ]" >> ${LOG_FILE} 
 
  else
 
    [ -n "${LOG_FILE}" ] && echo -e "\n${DATE} - Step ${STEP}. ERR - Distribution version not supported [ `os_id` ${OS_VERSION_ID} ]" >> ${LOG_FILE} 
    [ -n "${LOG_FILE}" ] && echo -e "${DATE} - Supported distribution: ${DISTR_SUPPORTED}" >> ${LOG_FILE} 
 
  fi
  
}

## ---------------- _multiOS_02 -------------- ##

disk_part_show ()
{
  # system disk - mount /boot dir
  ls /dev/sd*|grep sd[a-z] | grep -v "` df /boot/ |tail -1 | while read a b; do  echo $a ; done | sed -e 's/[0-9]//g'`"
  
}

disk_part_nofstab_show ()
{
  # system disk - mount /boot dir
  for d in {a..z}
  do
    if [ `ls /dev/sd${d}[0-9] 2>/dev/null | wc -l` -eq 0 ]
    then
      ls /dev/sd${d} 2>/dev/null | grep -v "` df /boot/ |tail -1 | while read a b; do  echo $a ; done | sed -e 's/[0-9]//g'`" | while read slice
      do
        # only missing in fstab
        egrep "^${slice}[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 || echo ${slice}
      done
    else
      ls /dev/sd${d}[0-9] 2>/dev/null | grep -v "` df /boot/ |tail -1 | while read a b; do  echo $a ; done | sed -e 's/[0-9]//g'`" | while read slice
      do
        # only missing in fstab
        egrep "^${slice}[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 || echo ${slice}
      done
    fi          
  done
}

convert_human_bitesS2MbitsS ()
{
  # bites/s -> Mbit/s
  echo -n `echo "$1 / ( 1000 * 1000 )" | bc` 

}

convert_system_MbitsS2bitesS ()
{
  # Mbit/s -> bites/s
  echo -n `echo "$1 * ( 1000 * 1000 )" | bc` 
  
}

convert_human_bytes2Gb ()
{
  # bytes -> Gbytes
  echo -n `echo "$1 / ( 1000 * 1000 * 1000 )" | bc` 

}

convert_system_Gb2bytes ()
{
  # Gbytes -> bytes
  echo -n `echo "$1 * ( 1000 * 1000 * 1000 )" | bc`
}

bandwidth_human ()
{
  # bites/s -> Mbits/s
  convert_human_bitesS2MbitsS "$1" 
  
}

bandwidth_system ()
{
  # bites/s -> Mbits/s
  convert_system_MbitsS2bitesS "$1" 
  
}
volume_human ()
{
  # bytes -> Gbytes
  convert_human_bytes2Gb "$1" 

}

volume_system ()
{
  # Gbytes -> bytes
  convert_system_Gb2bytes "$1" 

}

mountpoint_dir ()
{
  # mountpoit  
  [ -d $1 ] &&  df ${1} | tail -1 | while read Filesystem c1Kblocks Used Available Use Mounted ; do echo $Mounted ; done || \
                echo '/' ;
  
}

filesystem_dir ()
{
  # mountpoit  
  [ -d $1 ] &&  df ${1} | tail -1 | while read Filesystem c1Kblocks Used Available Use Mounted ; do echo $Filesystem ; done || \
                echo '/' ;
  
}

# ----------------------------------------------------------------------
# -------------------------------- keys --------------------------------
# for Step 2: Assigning ECDSA keys to your server. You may use existing keys or generate new
# Generation/Select public/private keys - standart ECDSA
gen_keys()
{
  LANCH_GEN_KEY=0

  # if the keys are in the system
  if [ -f ${KEY_PATH}/${KEY_PRIVATE} -a -f ${KEY_PATH}/${KEY_PUBLIC} ]
  then
  
    echo -e ${inf} "The keys ${KEY_PATH}/${KEY_PRIVATE} and ${KEY_PATH}/${KEY_PUBLIC} are already on the server.\n Shall we use them?" ${def}
    break_point ug
  
  else
  
    LANCH_GEN_KEY=1
  
  fi

  # if required - generate keys
  if [ ${LANCH_GEN_KEY} -gt 0 ] 
  then
  
    # gen priv
    ${SU} - ${NODR_USER} -c "openssl ecparam -name ${KEY_TYPE} -genkey -noout -out ${KEY_PATH}/${KEY_PRIVATE}"
    # public
    ${SU} - ${NODR_USER} -c "openssl ec -in ${KEY_PATH}/${KEY_PRIVATE} -pubout -out ${KEY_PATH}/${KEY_PUBLIC}"    
  
  fi
  
}

reg_public_key()
{
  echo -e "${inf}Please copy the entire phrase with you public key, including headline \"-----BEGIN PUBLIC KEY-----\" and bottom line \"-----END PUBLIC KEY-----\".${def}"
  echo -e "${inf}Then proceed to the NODR dashboard at https://dash.nodr.app and register it there.${def}"
  echo -e "${inf}DO NOT REGISTER YOUR SECRET KEY! IT MUST REMAIN ON YOUR NODR.${def}"
  echo -e "${inf} Shall we continue?${def}"
  
  NEXT_STEP=${STEP}
  while [ ${NEXT_STEP} -eq ${STEP} ] 
  do 

    break_point on ;

    # API - chk created NODR
    case "`check_public_key`" in
    
      "true")
        [ ${DEBUG_MODE} ] && echo -e "${inf}Congratulations! You've successfully bound your public key to your NODR dashboard!\n Shall we continue?${def}" 
        NEXT_STEP=`echo ${STEP} + 1 | bc` 
        continue
        ;;
        
      "false") 
        [ ${STEP} -lt 100 ] && echo -e "${inf}The public key isn't recognized by NODR backend.\n Please make sure that you've bound it correctly in the NODR dashboard and try again.\n In case is you need support please contact us via support@nodr.app.\n Shall we continue?${def}" 
        ;;
        
      "undef") 
        echo -e "${inf}${MSG_KEYS_NOT_RECOGNIZED}${def}" 
        ;;
        
      *) 
        echo -e "${inf}${MSG_NODR_BACKEND_NOACCESS}${def}" 
        ;;
    
    esac
    
  done
}


# ----------------------------------------------------------------------
# ------------------------------ HW info -----------------------------
# for Step 4: Hardware test. Your server must meet the minimum requirements of the NODR configuration you'll choose in the dashboard
# CPU
get_cpu_info ()
{
  # CPU in MHz
  CPU_MODEL=`grep "model" /proc/cpuinfo |grep -v "model name" | head -1 | cut -d':' -f2 | sed -e "s/^\ //g"`
  CPU_MODEL_NAME=`grep "model name" /proc/cpuinfo | head -1 | cut -d':' -f2 | sed -e "s/^\ //g"`
  CPU_CORE=`grep "processor" /proc/cpuinfo | wc -l`
  CPU_FREQUENCY=`grep "cpu MHz" /proc/cpuinfo | head -1 | cut -d':' -f2 | sed -e "s/^\ //g"`
  CPU_FREQUENCY=`echo "scale=0; ${CPU_FREQUENCY} * 1000 * 1000 / 1" | bc`
  CPU_ALL_FREQUENCY=`echo ${CPU_FREQUENCY} \* ${CPU_CORE} | bc`
  
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - CPU CPU_MODEL: [ ${CPU_MODEL} ]" >> ${LOG_FILE}
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - CPU CPU_MODEL_NAME: [ ${CPU_MODEL_NAME} ]" >> ${LOG_FILE}
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - CPU CPU_CORE: [ ${CPU_CORE} ]" >> ${LOG_FILE}
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - CPU CPU_FREQUENCY Hz: [ ${CPU_FREQUENCY} ]" >> ${LOG_FILE}
  
}

# RAM
get_ram_info ()
{
  ## RAM in bytes - ### up x100 
  RAM_TOTAL=`lsmem -b | grep -i "^Total online memory:" | sed -e 's/[^0-9]//g'`
  RAM_TOTAL=`echo "${RAM_TOTAL} * ${CPAR} / 1" | bc`
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - RAM Gb [ ${RAM_TOTAL} ]" >> ${LOG_FILE} 

}

# BANDWIDTH
get_bandwidth_info ()
{
  # BANDWIDTH  in Mbytes/s -> bites/s x1000
  BANDWIDTH_UPLOAD=`speedtest-cli --bytes --no-download 2>/dev/null | grep "^Upload:"` ;
  
  if [ $? -eq 0 ]
  then
    BANDWIDTH_UPLOAD=`echo ${BANDWIDTH_UPLOAD} | sed -e 's/[^0-9.]//g'` ;
    BANDWIDTH_UPLOAD=`echo "${BANDWIDTH_UPLOAD} * 8 *1024 * 1024 * ${CPAR} / 1" | bc` ;
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - BANDWIDTH_UPLOAD bytes/s [ ${BANDWIDTH_UPLOAD} ]" >> ${LOG_FILE} ;
  else
    BANDWIDTH_UPLOAD=${BANDWIDTH_UPLOAD_MAX_LIMIT} ;
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - BANDWIDTH_UPLOAD bytes/s [ ${BANDWIDTH_UPLOAD} ]" >> ${LOG_FILE} ;
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. Service speedtest not available, please try again later" >> ${LOG_FILE} ; 
  fi
  
}

# get world ip
get_world_ip ()
{
  ${CURL} ifconfig.me/ip 2>/dev/null
}

# INET 
set_inet ()
{
  # INET
  NODR_RUN_IP=""
  REGEXP_LAN="inet 127.0.0.1|inet 192.168.|inet 172.1[6-9].|inet 172.2[0-9].|inet 172.3[0-2].|inet 10."

  echo -e "${inf} IP installed on the server${def}"

  WAN_LIST="`ip a |grep 'inet ' | grep global | egrep -v -e \"${REGEXP_LAN}\" | cut -d ' ' -f 6 | cut -d '/' -f 1 `"  ;
  LAN_LIST="`ip a |grep 'inet ' | grep global | egrep -e \"${REGEXP_LAN}\" | cut -d ' ' -f 6 | cut -d '/' -f 1 `"     ;
      
  if [ "${WAN_LIST}" != "" ] 
  then
    # white ip on the server
    NUM_NODR_RUN_IP=1 ;
    WAN_LIST_ARRAY=(${WAN_LIST}) ;
    
    # list wan ip - "Array items and indexes"
    echo -e "${inf} WAN${def}" ;
    for index in ${!WAN_LIST_ARRAY[*]}
    do
      printf "%4d) %s\n" `echo ${index} + 1 | bc` ${WAN_LIST_ARRAY[$index]} ;  
    done
   
    # cycle select 
    while [ 1 -eq 1 ]
    do 
      echo -n -e "${act} Enter number IP for run NODR [ ${NUM_NODR_RUN_IP} ]: ${def}" ;
      read NUM_NODR_RUN_IP_READ ;
      echo "" ;
      
      [ "${NUM_NODR_RUN_IP_READ}" = "" ] && NUM_NODR_RUN_IP_READ=${NUM_NODR_RUN_IP} ;
    
    
      # test digit && in diapason
      if [[ ${NUM_NODR_RUN_IP_READ} =~ ${REGEX_DIGIT} ]] && [ ${NUM_NODR_RUN_IP_READ} -ge 1 -a ${NUM_NODR_RUN_IP_READ} -le ${#WAN_LIST_ARRAY[*]} ]
      then
        index=`echo ${NUM_NODR_RUN_IP_READ} - 1 | bc` ;
        NODR_RUN_IP=${WAN_LIST_ARRAY[$index]}   ;
        break ;
      else
        echo -e "${inf} Selection ${key}${NUM_NODR_RUN_IP_READ} ${def}${inf}is out of range${def}" ;
        index=`echo ${NUM_NODR_RUN_IP} - 1 | bc`    ;
        NODR_RUN_IP=${WAN_LIST_ARRAY[$index]}  ;
      fi
    done
    
    NODR_IP=${NODR_RUN_IP}
    echo -e "${inf} IP for internet connection to NODR ${NODR_IP} apply${def}" ;
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - INET supported, NODR_RUN_IP = NODR_IP  [ ${NODR_RUN_IP} ]" >> ${LOG_FILE} ;

  else
    # only LAN ip on the server
    NODR_IP="`get_world_ip`"
    
    NUM_NODR_RUN_IP=1 ;
    LAN_LIST_ARRAY=(${LAN_LIST}) ;
    
    # list wan ip - "Array items and indexes"
    echo -e "${inf} LAN${def}" ; 
    #echo "${LAN_LIST}" ;
    for index in ${!LAN_LIST_ARRAY[*]}
    do
      printf "%4d) %s\n" `echo ${index} + 1 | bc` ${LAN_LIST_ARRAY[$index]} ;  
    done

    # cycle select 
    while [ 1 -eq 1 ]
    do 
      echo -n -e "${act} Choose internal IP for NODR [${key} ${NUM_NODR_RUN_IP} ${act}]: ${def}" ;
      read NUM_NODR_RUN_IP_READ ;
      echo "" ;
      
      [ "${NUM_NODR_RUN_IP_READ}" = "" ] && NUM_NODR_RUN_IP_READ=${NUM_NODR_RUN_IP} ;

      # test digit && in diapason
      if [[ ${NUM_NODR_RUN_IP_READ} =~ ${REGEX_DIGIT} ]] && [ ${NUM_NODR_RUN_IP_READ} -ge 1 -a ${NUM_NODR_RUN_IP_READ} -le ${#LAN_LIST_ARRAY[*]} ]
      then
        index=`echo ${NUM_NODR_RUN_IP_READ} - 1 | bc` ;
        NODR_RUN_IP=${LAN_LIST_ARRAY[$index]} ;
        break ;
      else
        echo -e "${inf}Selection ${key}${NUM_NODR_RUN_IP_READ} ${def}${inf}is out of range${def}" ;
        index=`echo ${NUM_NODR_RUN_IP} - 1 | bc` ;
        NODR_RUN_IP=${LAN_LIST_ARRAY[$index]} ;
      fi
    done
    
    echo -e "${inf}Internal IP for NODR is ${NODR_RUN_IP}${def}" ;
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - INET supported, NODR_RUN_IP [ ${NODR_RUN_IP} ]" >> ${LOG_FILE} ;
    
    
    echo -n -e "${act}\n Enter IP for internet connection to NODR [${key} ${NODR_IP} ${act}]: ${def}" ;
    read NODR_IP_READ ;
    echo "" ;
    
    [ "${NODR_IP_READ}" != "" ] && NODR_IP=${NODR_IP_READ}
    echo -e "${inf}IP for internet connection to NODR ${key}${NODR_IP}${inf} apply${def}" ;
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - INET supported, NODR_IP [ ${NODR_IP} ]" >> ${LOG_FILE} ;   
    
  fi
  

  # PORT
  NODR_PORT=${NODR_PORT_DEFAULT}
  
  netstat -na|grep LISTEN | grep ":${NODR_PORT} " 1>/dev/null 2>&1
  [ $? -eq 0 ] && \
  echo -e "${inf}Default NODR port ${key}${NODR_PORT}${def}${inf} is in use on the server, please choose another.${def}"  || \
  echo -e "${inf}Default NODR port ${key}${NODR_PORT}${def}${inf} available for use on the server.${def}"  ;
  
  NEXT_STEP=${STEP} ;
  while [ ${NEXT_STEP} -eq ${STEP} ]
  do
 
    echo -n -e "${act}Enter port for NODR [${key} ${NODR_PORT} ${act}]: ${def}"  ;
    read NODR_PORT_READ ;
    echo "" ;

    case ${NODR_PORT_READ} in
      +([0-9])|"" ) # digit

        [ "${NODR_PORT_READ}" = "" ] && NODR_PORT=${NODR_PORT_DEFAULT} || NODR_PORT=${NODR_PORT_READ} ;
        
        if [ ${NODR_PORT} -ge 1024 -a ${NODR_PORT} -le 65535 ]
        then
        
          netstat -na|grep LISTEN | grep ":${NODR_PORT} " 1>/dev/null 2>&1  ;
          
          if [ $? -eq 0 ] 
          then    
          
            echo -e "${inf}NODR port ${key}${NODR_PORT}${def}${inf} is in use on the server, please choose another.${def}" ;
            NODR_PORT=${NODR_PORT_DEFAULT} ;
          
          else
          
            echo -e "${inf}NODR port ${key}${NODR_PORT}${def}${inf} available for use on the server.${def}" ;
            NEXT_STEP=`echo ${STEP} + 1 | bc` ;
            continue ;   
          
          fi
          
        else
        
          echo -e "${inf}Selection ${key}${NODR_PORT_READ} ${def}${inf}is out of range 1024-65535${def}" ; 
          NODR_PORT=${NODR_PORT_DEFAULT} ;       
        
        fi
        ;;
        
      * )
        echo -e "${inf}Selection ${key}${NODR_PORT_READ} ${def}${inf}is out of range${def}" ;
        NODR_PORT=${NODR_PORT_DEFAULT} ;
        ;;
        
     esac
     
  done

  echo -e "${inf} NODR port ${key}${NODR_PORT}${def}${inf} apply${def}" ;
  
  NODR_RUN_PORT=${NODR_PORT}  ;  
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - PORT supported [ ${NODR_PORT} ]" >> ${LOG_FILE} ;
  
}

# HDD
set_storage_dir ()
{
  ## DIR - bytes - ### test x1000
  echo -n -e ${act}"Enter directory for NODR storage: ${def}"
  read NODR_HDD ;
  echo "" ;
  
  echo -e "${inf}NODR HDD ${NODR_HDD} apply${def}" ;
  [ ! -d ${NODR_HDD} ] && mkdir -p ${NODR_HDD}
  chown ${NODR_USER} ${NODR_HDD}
  
  NODR_HDD_SIZE=`df --block-size 1 ${NODR_HDD} | tail -1 | while read fs kbloks Used Available Other; do   echo $Available; done`
  NODR_HDD_SIZE=`echo "${NODR_HDD_SIZE} * 1000 / 1" | bc`
  
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - HDD supported [ ${NODR_HDD}: ${NODR_HDD_SIZE} bytes]" >> ${LOG_FILE} 
  
}

set_storage_hdd ()
{
  ## HDD - bytes - ### test x1000
  echo -e "${inf}  HDD not system installed on the server:${def}"
  ls /dev/sd*|grep sd[b-z]
  echo -e "${inf}You need to select devices for NODR.${def}"    
  echo -e "${info}The selected devices will be formatted in ext4 and ${fat}all data on them will be lost.${def}"
  echo -e ${act}"Enter HDD for NODR: ${def}"
  read NODR_HDD ;
  echo "" ;
  echo -e "${inf}  NODR HDD ${NODR_HDD} apply${def}" ;
  
  NEXT_STEP=${STEP}
  while [ ${NEXT_STEP} -eq ${STEP} ]
  do
  
    fdisk -l ${NODR_HDD} | grep "Disk ${NODR_HDD}:" 1>/dev/null 2>&1 && \
    NEXT_STEP=`echo ${STEP} + 1 | bc` && continue 
    
    echo -e "${inf}NODR hdd ${NODR_HDD} not available on the server - choose another${def}" ; 
    
    ls /dev/sd*|grep sd[b-z]
    echo -e "${inf}You need to select devices for NODR.${def}"    
    echo -e "${info}The selected devices will be formatted in ext4 and ${fat}all data on them will be lost!!!${def}"
    echo -e ${act}"Enter HDD for NODR: ${def}"
    read NODR_HDD ;
    echo "" ;
    
    echo -e "${inf}NODR HDD ${NODR_HDD} apply${def}" ;
    
  done
  
  NODR_HDD_SIZE=`fdisk -l ${NODR_HDD} | grep "${NODR_HDD}:" 2>/dev/null | cut -d' ' -f5`
  NODR_HDD_SIZE=`echo "${NODR_HDD_SIZE} * 1000 / 1" | bc`
  
  [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - HDD supported [ ${NODR_HDD}: ${NODR_HDD_SIZE} Gb]" >> ${LOG_FILE} 
  
}

# get multistore size
get_size_multistore ()
{
  NODR_MULTISTORE="`[ \"${NODR_STORAGE_DIR}\" ] && echo -n \"${NODR_STORAGE_DIR}\"``[ \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \",${NODR_STORAGE_DRIVE}\"``[ ! \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \"${NODR_STORAGE_DRIVE}\"`" ;
  NODR_MULTISTORE_SIZE=0 ;
  for segment in `echo -n ${NODR_MULTISTORE} | sed -e "s/,/\ /g"`
  do
    segment_size=`echo ${segment} | cut -d':' -f2` ;
    NODR_MULTISTORE_SIZE=`echo "${NODR_MULTISTORE_SIZE} + ${segment_size}" | bc` ;
  done
  
  echo -n "${NODR_MULTISTORE_SIZE}" ;
}

# set storage multi store
set_storage_multistore ()
{
  STAGE_MULTISTORE=1; 

  NEXT_STEP=${STEP}
  while [ ${NEXT_STEP} -eq ${STEP} ]
  do 
    
    # level 0
    case ${STAGE_MULTISTORE} in
    
      1|"S"|"s")  # stage specifying the storage location
        _STAGE_MULTISTORE=${STAGE_MULTISTORE} ;
        NODR_MULTISTORE="`[ \"${NODR_STORAGE_DIR}\" ] && echo -n \"${NODR_STORAGE_DIR}\"``[ \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \",${NODR_STORAGE_DRIVE}\"``[ ! \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \"${NODR_STORAGE_DRIVE}\"`" ;
        
        NODR_MULTISTORE_SIZE=`get_size_multistore` ;
          
        # change "Directory", "Partition on disk", "Drive", "Exit"
        [ "${NODR_STORAGE_DIR}" != "" -o "${NODR_STORAGE_DRIVE}" != "" ] && echo -e  "${def}Current a storage location"
        [ "${NODR_STORAGE_DIR}" != "" ] &&                                  echo -e  "${def}  Directory:        ${key}${NODR_STORAGE_DIR} ${def}"   
        [ "${NODR_STORAGE_DRIVE}" != "" ] &&                                echo -e  "${def}  Parition/Drive:   ${key}${NODR_STORAGE_DRIVE} ${def}"
        [ "${NODR_MULTISTORE}" != "" ] &&                                   echo -e  "${def}  MULTISTORE:       ${key}${NODR_MULTISTORE} ${def}"
        [ "${NODR_STORAGE_DIR}" != "" -o "${NODR_STORAGE_DRIVE}" != "" ] && echo -e  "${def}  MULTISTORE_SIZE:  ${key}`volume_human ${NODR_MULTISTORE_SIZE}` Gb${def}"
        echo -e  "${inf}Please choose the location type you'd like to use for storage"
        echo -e  "${key}1 - D${act}irectory, ${key}2 - P${act}artition on Disk/D${key}r${act}ive, ${key}3 - N${act}o, please stop!${def}"
        echo -en "${act}Your choice [${key} 1 ${act}] : ${def}" ; 
        read _ANSWER ;
        echo "" ;
        
        # level 1
        case ${_ANSWER} in
        
          1|"D"|"d"|'') 
            # control absolute directory path
            while [ 1 -eq 1 ]
            do
            
              echo -en "${act}Enter directory for NODR storage: ${def}" ;
              read NODR_STORAGE_DIR_USER ;
              echo "" ;
              
              case ${NODR_STORAGE_DIR_USER} in
                
                /|/usr|/var|/boot|/dev|/lib|/lib64|/tmp|/run)
                 echo -e "${err}Incorrect ${key}${NODR_STORAGE_DIR_USER}${err} - system directory${def}";
                  continue ;
                  ;;
                   
                /[0-9a-zA-Z_-]*)
                  break ;
                  ;;
                
                *)
                  echo -e "${err}Incorrect absolute directory path${def}";
                  continue ;
                  ;;
                  
              esac
      
            done
            
            # If the directory is not a partition or disk mount point, go to the "Directory" stage
            ( ! grep "[[:blank:]]${NODR_STORAGE_DIR_USER}[[:blank:]]" /etc/fstab 1>/dev/null 2>&1     && \
              ! grep "[[:blank:]]${NODR_STORAGE_DIR_USER}/[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 )  && \
            STAGE_MULTISTORE=2 && continue ;
            ##echo -e "${inf}Directory ${key}${NODR_STORAGE_DIR_USER} ${inf}is not a partition or disk mount point${def}" 
            
         
            # If the directory is the mount point of a partition or drive, display the dialog "The partition/drive referenced by the directory will be formatted."
            if ( egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 || \
                 egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}/[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 )
            then
              # leave it unchanged 1046
              STAGE_MULTISTORE=2 && continue ;
              
              echo -e  "${inf} The partition/drive referenced by the directory ${key}${NODR_STORAGE_DIR_USER} ${inf}will be formatted!!!" ;
              
              while [ 1 -eq 1 ]
              do
              
                echo -en "${key}1 - Y${act}es, continue, or ${key}2 - N${act}o, please stop [${key} y ${def}] :" ;
                read __ANSWER ;
                echo "" ;
                
                # level 2
                case ${__ANSWER} in
              
                  1|"y"|"Y"|'') 
                    # part
                    ( egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}[[:blank:]]"  /etc/fstab 1>/dev/null 2>&1   || \
                      egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}/[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 ) && \
                    ( egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}[[:blank:]]"  /etc/fstab |  egrep "sd[a-z][1-9][[:blank:]]" 1>/dev/null 2>&1    || \
                      egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}/[[:blank:]]" /etc/fstab |  egrep "sd[a-z][1-9][[:blank:]]" 1>/dev/null 2>&1 )  && \
                    NODR_STORAGE_DRIVE_USER="" && \
                    STAGE_MULTISTORE=3 && \
                    break ;

                    # disk
                    ( egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}[[:blank:]]"  /etc/fstab 1>/dev/null 2>&1 || \
                      egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}/[[:blank:]]" /etc/fstab 1>/dev/null 2>&1 ) && \
                    ( egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}[[:blank:]]"  /etc/fstab |  egrep "sd[a-z][[:blank:]]" 1>/dev/null 2>&1     || \
                      egrep "[[:blank:]]${NODR_STORAGE_DIR_USER}/[[:blank:]]" /etc/fstab |  egrep "sd[a-z][[:blank:]]" 1>/dev/null 2>&1 )   && \
                    NODR_STORAGE_DRIVE_USER="" && \
                    STAGE_MULTISTORE=3 && \
                    break ;
                    ;;
 
                  2|"n"|"N") 
                    STAGE_MULTISTORE=1; 
                    break ;
                    ;;
                               
                  *) 
                    echo -e "${inf} Selection ${key}${__ANSWER} ${def}${inf}is out of range${def}" ; 
                    continue ;
                    ;;
                  
                esac
              
              done  
              
            fi 
            
            # change STAGE_MULTISTORE
            [ ${STAGE_MULTISTORE} -ne ${_STAGE_MULTISTORE} ] && continue ;   
            ;;
            
          2|"P"|"p"|"R"|"r") 
          
            echo -e "${inf} WARNING! The Partition/Drive will be formatted. All data will be permanently deleted. Are you sure?${def}" ;
            
            while [ 1 -eq 1 ]
            do
            
              echo -en "${key}1 - Y${act}es, I'm ready to format the Partition/Drive, or ${key}2 - N${act}o, please stop [${key} y ${act}]: ${def}" ;
              read __ANSWER ;
              echo "" ;

              case ${__ANSWER} in
            
                1|"y"|"Y"|'') 
                  # select a list of partitions/drives (in which system or previously selected partitions and drives should not be displayed)
                  # system disk - mount /boot dir
                  [ `disk_part_nofstab_show |wc -l` -gt 0 ]       && \
                  echo -e "${inf}List partitions/drives: ${def}"  && \
                  disk_part_nofstab_show ;
                  
                  # test available
                  [ `disk_part_nofstab_show |wc -l` -eq 0 ] && \
                  echo -e "${err}No available free Partitions/Drive for NODR.${def} " && \
                  STAGE_MULTISTORE=1 && \
                  break ; 
                  
                  while [ 1 -eq 1 ]
                  do
                    echo -en "${act} Enter Partition/Drive for NODR storage: ${def}"
                    read NODR_STORAGE_DRIVE_USER ;
                    echo "" ;
                    
                    case ${NODR_STORAGE_DRIVE_USER} in
                    
                      '')
                        echo -e "${inf} Selection ${key}${NODR_STORAGE_DRIVE_USER} ${def}${inf}is out of range${def}" ; 
                        continue ;
                        ;;                  
                  
                    esac
                    
                    disk_part_nofstab_show | grep "^${NODR_STORAGE_DRIVE_USER}$" 1>/dev/null 2>&1 && \
                    break ||
                    echo -e "${inf} Selection ${key}${NODR_STORAGE_DRIVE_USER} ${def}${inf}is out of range${def}" 
                    
                  done
                  
                  NODR_STORAGE_DIR_USER="" ;
                  STAGE_MULTISTORE=3 ;
                  
                  break ;
                  ;;

                2|"n"|"N") 
                  STAGE_MULTISTORE=1; 
                  break ;
                  ;;

                *) 
                  echo -e "${inf} Selection ${key}${__ANSWER} ${def}${inf}is out of range${def}" ; 
                  continue ;
                  ;;
                
              esac

            done
            ;;

          3|"E"|"e")
            STAGE_MULTISTORE=4 ;
            ;;   
           
          *) 
            echo -e "${inf} Selection ${key}${_ANSWER} ${def}${inf}is out of range${def}" ; 
            continue ;
            ;;

        esac 
        ;;
        
      # level 0
      2|"D"|"d") 
        # Stage directory
        _STAGE_MULTISTORE=${STAGE_MULTISTORE} ;
         
        [ ! -d ${NODR_STORAGE_DIR_USER} ] && mkdir -p ${NODR_STORAGE_DIR_USER} && chown ${NODR_USER} ${NODR_STORAGE_DIR_USER}
        
        # control double use mountpoint
        if [ "${NODR_STORAGE_DIR}" != "" ]
        then
        
          for segment in `echo "${NODR_STORAGE_DIR}" | sed 's/\,/\ /g'`
          do
          
            dir_segment=`echo ${segment} | cut -d':' -f1` ;
            if [ "`mountpoint_dir ${dir_segment}`" = "`mountpoint_dir ${NODR_STORAGE_DIR_USER}`" ]
            then
            
              echo -e "${err}Mount point ${key}`mountpoint_dir ${NODR_STORAGE_DIR_USER}` for ${NODR_STORAGE_DIR_USER}${err} reuse not allowed !!!${def}" ;
              STAGE_MULTISTORE=1 ;        
              break ;
                            
            fi
            
          done
           
        fi 
        
        # control double use mountpoint
        if [ "${NODR_STORAGE_DRIVE}" != "" ]
        then
        
          for segment in `echo "${NODR_STORAGE_DRIVE}" | sed 's/\,/\ /g'`
          do
          
            dir_segment=`echo ${segment} | cut -d':' -f1` ;
            if [ "`mountpoint_dir ${dir_segment}`" = "`mountpoint_dir ${NODR_STORAGE_DIR_USER}`" ]
            then
            
              echo -e "${err}Mount point ${key}`mountpoint_dir ${NODR_STORAGE_DIR_USER}` for ${NODR_STORAGE_DIR_USER}${err} reuse not allowed !!!${def}" ;
              STAGE_MULTISTORE=1 ;        
              break ;
                            
            fi
            
          done
        
        fi
        
        # change STAGE_MULTISTORE
        [ ${STAGE_MULTISTORE} -ne ${_STAGE_MULTISTORE} ] && continue ;         
       
        # dir on /
        if ( df ${NODR_STORAGE_DIR_USER} | egrep "/$" 1>/dev/null 2>&1 ) 
        then
        
          echo -e "${inf} It is not recommended to place storage on the system drive. Continue ?" ;
            
          while [ 1 -eq 1 ]
          do
            
            echo -en "${key}1 - Y${act}es, continue, or ${key}2 - N${act}o, please stop [${key} n ${act}]: ${def}" ;
            read _ANSWER ;
            echo "" ;

            case ${_ANSWER} in
            
              1|"y"|"Y") 
                break ;
                ;;
            
              2|"n"|"N"|'') 
                STAGE_MULTISTORE=1; 
                break ;
                ;;
                
              *) 
                echo -e "${inf} Selection ${key}${_ANSWER} ${def}${inf}is out of range${def}" ; 
                continue ;
                ;;
                
            esac 
            
          done 

        fi
        
        # change STAGE_MULTISTORE
        [ ${STAGE_MULTISTORE} -ne ${_STAGE_MULTISTORE} ] && continue ; 
          
        # size read KB all capacity x1000
        echo -e -n "${inf}Getting location volume info ... ${def}" ;
        _SIZE=`df ${NODR_STORAGE_DIR_USER} | tail -1 | while read fs kbloks Used Available Other; do   echo $kbloks; done`
        NODR_STORAGE_DIR_LIMIT=`echo "${_SIZE} * 1024 * ${CPAR}" | bc` ;
        NODR_STORAGE_DIR_LIMIT_HUMAN=`volume_human ${NODR_STORAGE_DIR_LIMIT}` ;
        echo -e "${inf}done.${def}" ;
            
        echo -e  "${inf}The measured location ${NODR_STORAGE_DIR_USER} volume is${key} ${NODR_STORAGE_DIR_LIMIT_HUMAN}${inf} Gb.${def}" ;
        
        while [ 1 -eq 1 ]
        do
        
          echo -e  "${inf} Shall NODR use all measured volume, or you'd like to set a volume limit?"
          echo -e  "${key}1 - O${act}k, let NODR use all of it, or ${key}2 - N${act}o, let's set a limit${def}"
          echo -en "${act}Your choice [${key} 1 ${act}]: ${def}" ; 

          read _ANSWER ;
          echo "" ;
    
          case ${_ANSWER} in
        
            1|"O"|"o"|"") 
              [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ${NODR_STORAGE_DIR_USER} - No limit used." >> ${LOG_FILE} ;
              ;;
        
            2|"N"|"n")
              _NEXT_STEP=${STEP}
              while [ ${_NEXT_STEP} -eq ${STEP} ]
              do
            
                echo -e -n "${act}Limit volume for storage Gb [${key} ${NODR_STORAGE_DIR_LIMIT_HUMAN} ${act}] : ${def}"
                read __ANSWER ;
                echo "" ;
              
                case ${__ANSWER} in
        
                  '') 
                    _NEXT_STEP=`echo ${STEP} + 1 | bc` && continue ;
                    ;;
                  
                  [0-9]|[0-9][0-9]|[0-9][0-9][0-9]|[0-9][0-9][0-9][0-9]|[0-9]* ) 
                    [ ${__ANSWER} -gt 0 -a ${__ANSWER} -le ${NODR_STORAGE_DIR_LIMIT_HUMAN} ] && NODR_STORAGE_DIR_LIMIT=`volume_system ${__ANSWER}` && _NEXT_STEP=`echo ${STEP} + 1 | bc` && continue || \
                    echo -e "${inf}Incorrect volume! Please try again.${def}" ;
                    ;;
            
                  * ) echo -e "${inf}Incorrect volume! Please try again.${def}" ;
                    ;;
        
                esac
              
              done
              ;;
            
            *) echo -e "${inf}Incorrect ${_ANSWER} value entered.${def}" ;
              continue ;
              ;;
        
          esac
          
          break ;

        done          

        # 
        NODR_STORAGE_DIR="`[ \"${NODR_STORAGE_DIR}\" ] && echo -n \"NODR_STORAGE_DIR,\"`${NODR_STORAGE_DIR_USER}:${NODR_STORAGE_DIR_LIMIT}" ;
        
        NODR_MULTISTORE_SIZE=`get_size_multistore` ;
        NODR_MULTISTORE="`[ \"${NODR_STORAGE_DIR}\" ] && echo -n \"${NODR_STORAGE_DIR}\"``[ \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \",${NODR_STORAGE_DRIVE}\"``[ ! \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \"${NODR_STORAGE_DRIVE}\"`" ;
        
        echo -e  "${inf} Aggregated storage volume is `volume_human ${NODR_MULTISTORE_SIZE}` Gb. Shall we add new location to storage?${def}" ; 
        echo -e  "${key}1 - Y${act}es, let's add next location, or ${key}2 - N${act}o, we're finished${def}"
        echo -en "${act}Your choice [${key} 1 ${act}]: ${def}" ; 
        read _ANSWER ;
        echo "" ;
    
        case ${_ANSWER} in
          1|"Y"|"y"|"")
            STAGE_MULTISTORE=1 ;   # stage specifying the storage location
            ;;
            
          2|"N"|"n")
            STAGE_MULTISTORE=4 ;   # stage finish configure location
            ;;

          * ) echo -e "${inf}Incorrect ${_ANSWER} value entered.${def}" ;
            ;;
            
        esac                   
        
        ;;
      
      # level 0
      3|"P"|"p"|"R"|"r")
        ## Stage "Partition on disk or Disk"  
        # 1) system partition / disk - filtred on select
        
        # NODR_STORAGE_DIR_USER - part / disk
        if [ "${NODR_STORAGE_DRIVE_USER}" = "" -a "${NODR_STORAGE_DIR_USER}" != "" ]
        then
          
          _NODR_STORAGE_DRIVE_USER=`filesystem_dir ${NODR_STORAGE_DIR_USER}`;
          
          # 2) unmount the partition/disk if it is mounted.
          mount | egrep "^${_NODR_STORAGE_DRIVE_USER}[[:blank:]]" 1>/dev/null 2>&1 && umount ${_NODR_STORAGE_DRIVE_USER} ;
          
          # 3) comment to fstab
          __NODR_STORAGE_DRIVE_USER=`echo ${_NODR_STORAGE_DRIVE_USER} | sed -e 's/\//\\\//g'`
          sed -i "s/^${__NODR_STORAGE_DRIVE_USER}/\#${__NODR_STORAGE_DRIVE_USER}/" /etc/fstab ;
          
          # 4) set NODR_STORAGE_DRIVE_USER
          NODR_STORAGE_DRIVE_USER=${_NODR_STORAGE_DRIVE_USER} ;   
          
        fi
        
        if [ "${NODR_STORAGE_DRIVE_USER}" != "" ]
        then        
          # 2) unmount the partition/disk if it is mounted.
          mount | egrep "^${NODR_STORAGE_DRIVE_USER}[[:blank:]]" 1>/dev/null 2>&1 && umount ${NODR_STORAGE_DRIVE_USER} ;
        
          # 3) create an ext4 file system on a partition/disk using the mkfs.ext4 <partition/disk device> command.
          mkfs.ext4 -F ${NODR_STORAGE_DRIVE_USER} 
        
          # 4) set the number of spare blocks for a partition/disk to 0 using tune2fs -r 0 <partition/disk device>
          tune2fs -r 0 ${NODR_STORAGE_DRIVE_USER}
        
          # 5) create a directory in the service user's home directory in the storageN format.
          NODR_STORAGE_DRIVE_COUNT=`echo ${NODR_STORAGE_DRIVE} | sed -e 's/[^:]//g' |wc -c`
          [ ! -d /mnt/${NODR_DIR}/storage${NODR_STORAGE_DRIVE_COUNT} ] && mkdir -p /mnt/${NODR_DIR}/storage${NODR_STORAGE_DRIVE_COUNT}
                
          # 6) add a line to /etc/fstab for mounting in the format: <partition/disk device> <directory from step 5> ext4 noatime,nodiratime,noacl,data=writeback,commit=15,barrier=0 0 0
          echo -e "${NODR_STORAGE_DRIVE_USER}\t/mnt/${NODR_DIR}/storage${NODR_STORAGE_DRIVE_COUNT}\text4\tnoatime,nodiratime,noacl,data=writeback,commit=15,barrier=0\t0\t0" | tee -a ${OS_FSTAB} && \
        
          # 7) mount the partition/disk using the directory from step 5 as the mount point.
          mount ${NODR_STORAGE_DRIVE_USER} && \
          chown ${NODR_USER} /mnt/${NODR_DIR}/storage${NODR_STORAGE_DRIVE_COUNT} && \
          [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - ${NODR_STORAGE_DRIVE_USER} mounted  [ ${NODR_STORAGE_DRIVE_USER} mounted ]" >> ${LOG_FILE} || \
          ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - ${NODR_STORAGE_DRIVE_USER} not mounted  [ ${NODR_STORAGE_DRIVE_USER} not mounted ]" >> ${LOG_FILE} )
        
          # 8) size KB all capacity x1000
          echo -e -n "${inf}Getting location volume info ... ${def}" ;
          _SIZE=`df /mnt/${NODR_DIR}/storage${NODR_STORAGE_DRIVE_COUNT} | tail -1 | while read fs kbloks Used Available Other; do   echo $kbloks ; done`
          NODR_STORAGE_DRIVE_LIMIT=`echo "${_SIZE} * 1024 * ${CPAR}" | bc` ;
          NODR_STORAGE_DRIVE_LIMIT_HUMAN=`volume_human ${NODR_STORAGE_DRIVE_LIMIT}` ;
          echo -e "${inf}done.${def}" ;
        
          # 9) limit size           
          echo -e  "${inf}The measured location ${NODR_STORAGE_DRIVE_USER} volume is ${key}${NODR_STORAGE_DRIVE_LIMIT_HUMAN}${inf} Gb.${def}" ;
          
          while [ 1 -eq 1 ]
          do
            echo -e  "${inf} Shall NODR use all measured volume, or you'd like to set a volume limit?"
            echo -e  "${key}1 - O${act}k, let NODR use all of it, or ${key}2 - N${act}o, let's set a limit${def}"
            echo -en "${act}Your choice [${key} 1 ${act}]: ${def}" ; 

            read _ANSWER ;
            echo "" ;
    
            case ${_ANSWER} in
          
              1|"O"|"o"|"") 
                [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ${NODR_STORAGE_DRIVE_USER} - No limit used." >> ${LOG_FILE} ;
                break ;
                ;;
          
              2|"N"|"n")
                _NEXT_STEP=${STEP}
                while [ ${_NEXT_STEP} -eq ${STEP} ]
                do
              
                  echo -e -n "${act}Limit volume for storage Gb [ ${NODR_STORAGE_DRIVE_LIMIT_HUMAN} ] : ${def}"
                  read __ANSWER ;
                  echo "" ;

                  case ${__ANSWER} in
        
                    '') 
                      _NEXT_STEP=`echo ${STEP} + 1 | bc` && continue ;
                      ;;
                  
                    [0-9]|[0-9][0-9]|[0-9][0-9][0-9]|[0-9][0-9][0-9][0-9]|[0-9]* ) 
                      [ ${__ANSWER} -gt 0 -a ${__ANSWER} -le ${NODR_STORAGE_DRIVE_LIMIT_HUMAN} ] && NODR_STORAGE_DRIVE_LIMIT=`volume_system ${__ANSWER}` && _NEXT_STEP=`echo ${STEP} + 1 | bc` && continue || \
                      echo -e "${inf}Incorrect volume! Please try again.${def}" ;
                      ;;
            
                    * ) echo -e "${inf}Incorrect volume! Please try again.${def}" ;
                      ;;
        
                  esac
            
                done
                break ;
                ;;
                
              *) echo -e "${inf}Incorrect ${_ANSWER} value entered.${def}" ;
              continue ;
              ;;
                    
          esac
          
        done          

        fi

        NODR_STORAGE_DRIVE="`[ \"${NODR_STORAGE_DRIVE}\" ] && echo -n \"${NODR_STORAGE_DRIVE},\"`/mnt/${NODR_DIR}/storage${NODR_STORAGE_DRIVE_COUNT}:${NODR_STORAGE_DRIVE_LIMIT}" 
        
        NODR_MULTISTORE_SIZE=`get_size_multistore` ;
        NODR_MULTISTORE="`[ \"${NODR_STORAGE_DIR}\" ] && echo -n \"${NODR_STORAGE_DIR}\"``[ \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \",${NODR_STORAGE_DRIVE}\"``[ ! \"${NODR_STORAGE_DIR}\" -a \"${NODR_STORAGE_DRIVE}\" ] && echo -n \"${NODR_STORAGE_DRIVE}\"`" ;
        
        echo -e  "${inf} Aggregated storage volume is `volume_human ${NODR_MULTISTORE_SIZE}` Gb. Shall we add new location to storage?${def}" ; 
        echo -e  "${key}1 - Y${act}es, let's add next location, or ${key}2 - N${act}o, we're finished${def}"
        echo -en "${act}Your choice [${key} 1 ${act}]: ${def}" ; 
        read _ANSWER ;
        echo "" ;
    
        case ${_ANSWER} in
          1|"Y"|"y"|"")
            STAGE_MULTISTORE=1 ;   # stage specifying the storage location
            ;;
            
          2|"N"|"n")
            STAGE_MULTISTORE=4 ;   # stage finish configure location
            ;;

          * ) echo -e "${inf}Incorrect ${_ANSWER} value entered.${def}" ;
            ;;
            
        esac                   
        
        ;;
      
      # level 0
      4|"E"|"e") 
        echo -e "${inf}The storage for NODR has been configured successfully.${def}" ;
        NODR_MULTISTORE_SIZE=`get_size_multistore` ;
        echo -e "${inf}Aggregated storage volume is `volume_human ${NODR_MULTISTORE_SIZE}` Gb" ; 
        NEXT_STEP=`echo ${STEP} + 1 | bc` ;
        ;;
        
    esac  
  
  done
  
}

create_storage_nodr ()
{
  if [ ${NODR_HDD} ] && mkfs.ext4 -F ${NODR_HDD} 
  then
  
    [ ${DEBUG_MODE} ] && echo -e "${inf}  Created storage${key} ${NODR_HDD}${def}"
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Create storage ${NODR_HDD} [ ${NODR_HDD} created ]" >> ${LOG_FILE} || \
    ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - not create storage ${NODR_HDD} [ ${NODR_HDD} not created ]" >> ${LOG_FILE} ) 
  
    if mount | grep "${NODR_HDD} " 1>/dev/null 2>&1
    then
  
      [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - ${NODR_HDD} already mounted  [ ${NODR_HDD} mounted ]" >> ${LOG_FILE}  
  
    else 
  
      echo -e "${NODR_HDD}\t${NODR_STORAGE_DIR}\text4\tnoatime,nodiratime,noacl,data=writeback,commit=15,barrier=0\t0\t0" | tee -a ${OS_FSTAB} && mount ${NODR_HDD} && \
      chown ${NODR_USER} ${NODR_STORAGE_DIR} && \
      [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - ${NODR_HDD} mounted  [ ${NODR_HDD} mounted ]" >> ${LOG_FILE} || \
      ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - ${NODR_HDD} not mounted  [ ${NODR_HDD} not mounted ]" >> ${LOG_FILE} )
  
    fi
  
  else
  
    [ ${DEBUG_MODE} ] && echo -e "${inf}  Not created storage${key}${NODR_HDD}${def}" ;
  
  fi   
}

# ----------------------------------------------------------------------
# -------------------------------- OS ---------------------------------
# for Step 8: NODR app automated installation.  
# break_point
break_point ()
{
  case $1 in
    "on")                 echo -e "${def}1 - O${act}k, go ahead, or ${def}2 - N${act}o, please stop.${def}";;
    "onr")                echo -e "${def}1 - O${act}k, go ahead, or ${def}2 - N${act}o, please stop!, or${def} 3 - R${act}econfigure.${def}";;
    "onr_check_config")   echo -e "${def}1 - O${act}k, go ahead, or ${def}2 - N${act}o, please stop!, or${def} 3 - R${act}econfigure NODR.${def}";;
    "yn")                 echo -e "${def}1 - Y${act}es, continue, or ${def}2 - N${act}o, please stop.${def}" ;;
    "yn_install")         echo -e "${def}1 - Y${act}es, install the NODR app, or ${def}2 - N${act}o, please stop!${def}" ;;
    "ynr_check_nodr_ip")  echo -e "${def}1 - Y${act}es, continue, or ${def}2 - N${act}o, please stop,${def} 3 - R${act}econfigure the network settings.${def}" ;;
    "ynr_set_storage")    echo -e "${def}1 - Y${act}es, continue, or ${def}2 - N${act}o, please stop,${def} 3 - R${act}econfigure the storage.${def}" ;;
    "ynr_set_active")     echo -e "${def}1 - Y${act}es, continue, or ${def}2 - N${act}o, please stop,${def} 3 - R${act}econfigure NODR.${def}" ;;
    "ug")                 echo -e "${def}1 - U${act}se them, or ${def}2 - G${act}enerate new keys.${def}" ;;
  esac      

  while [ 1 -eq 1 ]
  do
    echo -en "${act}Your choice [${key} 1 ${act}] : ${def}" ; 
    read ANSWER
    echo -e ""  ;
  
    case $1 in
      
      "on")
        case ${ANSWER} in
          1|"O"|"o"|'')  ;;
          2|"N"|"n") echo -e "You've termninated the installation procedure." && STEP=`echo ${STEP} + 100 | bc` ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;
        
      "onr")
        case ${ANSWER} in
          1|"O"|"o"|'')  ;;
          2|"N"|"n") echo -e "You've termninated the installation procedure." && STEP=`echo ${STEP} + 100 | bc` ;;
          3|"R"|"r") STEP=`echo ${STEP} - 1 | bc`  ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;

      "onr_check_config")
        case ${ANSWER} in
          1|"O"|"o"|'')  ;;
          2|"N"|"n") echo -e "You've termninated the installation procedure." && STEP=`echo ${STEP} + 100 | bc` ;;
          3|"R"|"r") STEP=4  ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;
                 
      "yn"|"yn_install")
        case ${ANSWER} in
          1|"Y"|"y"|'')  ;;
          2|"N"|"n") echo -e "You've termninated the installation procedure." && STEP=`echo ${STEP} + 100 | bc` ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;

      "ynr_check_nodr_ip"|"ynr_set_storage")
        case ${ANSWER} in
          1|"Y"|"y"|'')  ;;
          2|"N"|"n") echo -e "You've termninated the installation procedure." && STEP=`echo ${STEP} + 100 | bc` ;;
          3|"R"|"r") NEXT_STEP=${_STEP}; STEP=${_STEP} ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;

      "ynr_set_active")
        case ${ANSWER} in
          1|"Y"|"y"|'')  ;;
          2|"N"|"n") echo -e "You've termninated the installation procedure." && STEP=`echo ${STEP} + 100 | bc` ;;
          3|"R"|"r") STEP=4 ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;

      "ug")
        case ${ANSWER} in
          1|"U"|"u"|'') LANCH_GEN_KEY=0 ;;
          2|"G"|"g")    LANCH_GEN_KEY=2 ;;
          *) echo -e "${inf}  Selection ${key}${ANSWER} ${def}${inf}is out of range${def}" ; continue ;;
        esac
        ;;
        
    esac 
  
    break ;
    
  done
  
}  

## ---------------- multiOS_03 -------------- ##
# Add start pkg
add_start_pkg_almalinux ()
{
  # configure pkg
  yum upgrade libmodulemd -y
     
  for pkg in ${START_PKG_LIST} ${START_PKG_LIST_almalinux}
  do
   
    yum install ${pkg} -y
   
  done 
}

add_start_pkg_centos ()
{
  # upd repo
  sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
  sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://mirror.yandex.ru|g' /etc/yum.repos.d/CentOS-*
  yum upgrade libmodulemd -y
    
  for pkg in ${START_PKG_LIST} ${START_PKG_LIST_centos}
  do

    yum install ${pkg} -y

  done
}

add_start_pkg_debian ()
{
  # configure pkg
  dpkg --configure -a
     
  for pkg in ${START_PKG_LIST} ${START_PKG_LIST_debian}
  do
   
    apt install ${pkg} -y
   
  done 
}

add_start_pkg_ubuntu ()
{
  # configure pkg
  dpkg --configure -a

  # manual install
  wget http://archive.ubuntu.com/ubuntu/pool/main/l/lua5.3/liblua5.3-0_5.3.3-1.1ubuntu2_amd64.deb && yes | dpkg -i liblua*
  
  # auto
  for pkg in ${START_PKG_LIST} ${START_PKG_LIST_ubuntu}
  do
   
    apt install ${pkg} -y
   
  done 
 
}

# Upgrade OS
update_system_almalinux ()
{
  yum update -y && \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Upgrade successfully done  [ `os_id` upgrade done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Upgrade unsuccessfully done  [ `os_id` not upgrade ]" >> ${LOG_FILE} )
}

update_system_centos ()
{
  yum update -y --allowerasing --nobest --nogpgcheck  && \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Upgrade successfully done  [ `os_id` upgrade done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Upgrade unsuccessfully done  [ `os_id` not upgrade ]" >> ${LOG_FILE} )
}

update_system_debian ()
{
  apt update -y  && \
  apt upgrade -y && \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Upgrade successfully done  [ `os_id` upgrade done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Upgrade unsuccessfully done  [ `os_id` not upgrade ]" >> ${LOG_FILE} ) 
}

update_system_ubuntu ()
{
  apt update -y  && \
  apt upgrade -y && \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Upgrade successfully done  [ `os_id` upgrade done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Upgrade unsuccessfully done  [ `os_id` not upgrade ]" >> ${LOG_FILE} ) 
}

# Install system packages
install_pkg_almalinux ()
{
  # list system packages
  yum install ca-certificates curl net-tools iftop mc -y

  # wget repo
  cd /etc/yum.repos.d/
  wget -N https://download.docker.com/linux/centos/docker-ce.repo
  cd -

  # install docker
  yum install docker-ce -y

  systemctl enable docker
  systemctl start docker
}

install_pkg_centos ()
{
  # list system packages
  yum install ca-certificates curl software-properties-common net-tools iftop mc -y

  # wget repo
  cd /etc/yum.repos.d/
  wget -N https://download.docker.com/linux/centos/docker-ce.repo
  cd -

  # install docker
  yum install docker-ce -y

  systemctl enable docker
  systemctl start docker
}

install_pkg_debian ()
{
  # Update the apt package index and install packages to allow apt to use a repository over HTTPS
  apt-get update ;
  
  # list system packages
  apt install apt-transport-https ca-certificates curl gnupg lsb-release software-properties-common net-tools iftop mc -y ;

  # Add Docker’s official GPG key:
  [ ! -d /etc/apt/keyrings ] && mkdir -p /etc/apt/keyrings ;
  ${CURL} -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg ;


  # Use the following command to set up the repository:
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
  # Update the apt package index, and install the latest version of Docker Engine, containerd, and Docker Compose, or go to the next step to install a specific version:
  apt-get update ;
  
  # check correct repository Docker
  if apt-cache policy docker-ce  | grep "Candidate:" | grep "~debian" 1>/dev/null 2>&1
  then
  
    echo "Correct repository Docker" 
  
  else 
  
    echo "Not correct repository Docker" 
    exit 120
  
  fi

  # install docker
  apt install docker-ce -y
}

install_pkg_ubuntu ()
{
  # list system packages
  apt install apt-transport-https ca-certificates curl software-properties-common net-tools iftop mc -y

  # key GPG
  ${CURL} -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

  # repository Docker
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" -y

  # check correct repository Docker
  if apt-cache policy docker-ce  | grep "Candidate:" | grep "~ubuntu-focal" 1>/dev/null 2>&1
  then
  
    echo "Correct repository Docker" 
  
  else 
  
    echo "Not correct repository Docker" 
    exit 120
  
  fi

  # install docker
  apt install docker-ce -y
}

## ---------------- _multiOS_03 -------------- ##

check_docker_installed ()
{
  # Check running docker (start after install)
  if systemctl is-active --quiet service docker
  then
    echo "Docker running - active"
  else
    echo "Docker - error running - not active"
    exit 120
  fi

  # Default docker run from root only, add current user on to the group docker
  usermod -aG docker ${NODR_USER}

  # Check in group docker
  su - ${NODR_USER} -c groups | grep docker 1>/dev/null 2>&1 &&  \
    echo "OK - ${USER} successfulliy added to the group docker" || \
    "ERR - ${USER} unsuccessfulliy added to the group docker"

  # Install docker-compose
  # curl -L
  curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

  # chmod exec
  chmod +x /usr/local/bin/docker-compose

  # Check installation
  /usr/local/bin/docker-compose --version | grep ${DOCKER_COMPOSE_VERSION} 1>/dev/null 2>&1 &&              \
    echo "OK - successfulliy install ${DOCKER_COMPOSE_VERSION} docker-compose" || \
    echo "ERR - unsuccessfulliy install ${DOCKER_COMPOSE_VERSION} docker-compose"

  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Install system packages successfully done  [ `os_id` install system packages done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Install system packages unsuccessfully done  [ `os_id` not install system packages ]" >> ${LOG_FILE} )

}

# Configure env NODR
configure_env_nodr ()
{

  docker_compose 1>${NODR_DIR}/${NODR_DOCKER_COMPOSE} && \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Configure env NODR successfully done  [ env NODR done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Configure env NODR unsuccessfully done  [ env NODR not done ]" >> ${LOG_FILE} ) 

}

# Install NODR
install_nodr ()
{

  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Install NODR successfully done  [ Install NODR done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Install NODR unsuccessfully done  [ Install NODR not done ]" >> ${LOG_FILE} ) 

}

# Run NODR
run_nodr ()
{
  # run
  su - ${NODR_USER} -c "cd ${NODR_DIR} ; /usr/local/bin/docker-compose up -d" && \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Run NODR successfully done  [ Run NODR done ]" >> ${LOG_FILE} ) || \
  ( [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. ERR - Run NODR unsuccessfully done  [ Run NODR not done ]" >> ${LOG_FILE} ) 

}

# =========================================================================
# ------------------------------ main ------------------------------------
STEP=1

echo -e "\n${inf}${MSG_INFO_INSTALLER}${def}";
break_point on

while [ ${STEP} -le ${MAX_STEP} ]
do

case ${STEP} in

  1) ## Step 1. OS 
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Checking the OS version support.${def}"
    OS_SUPPORTED=0 ; 
    check_os_supported ;
    if [ ${OS_SUPPORTED} -gt 0 ] 
    then
    
      # add NODR_GROUP 
      grep "^${NODR_GROUP}:" /etc/group 1>/dev/null 2>&1 || groupadd ${NODR_GROUP}
      # add NODR_USER 
      grep "^${NODR_USER}:" /etc/passwd 1>/dev/null 2>&1 || useradd -m -g ${NODR_GROUP} -d ${NODR_USER_HOME} ${NODR_USER}  
      # create .ssh
      su - ${NODR_USER} -c "ssh-keygen -t rsa -N '' -f ~${NODR_USER}/.ssh/id_rsa << y" 2>/dev/null
        
      [ ${DEBUG_MODE} ] && echo -e "\n${inf}You OS version [ `os_id` ${OS_VERSION_ID} ] is supported.\n Shall we continue?${def}"
      break_point on
     
      STEP=`echo ${STEP} + 1 | bc` && continue 
      
    else
      [ ${DEBUG_MODE} ] && echo -e "${inf}Unfortunately, your OS version [ `os_id` ${OS_VERSION_ID} ] is not currently supported.${def}"
      [ ${DEBUG_MODE} ] && echo -e "${inf}Supported version: ${DISTR_SUPPORTED}${def}" 

      echo -e "${inf}The installation procedure is terminated."
      STEP=`echo ${STEP} + 100 | bc` && continue
      
    fi
    ;;

  2) # Step 2. Assigning ESDCA keys to server 
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Assigning ESDCA keys to server.${def}"
    gen_keys
     
    [ ${DEBUG_MODE} ] && echo -e "${inf}Congratulations! You've successfully assign ESDCA keys to server.\n Shall we continue?${def}"
    
    STEP=`echo ${STEP} + 1 | bc` ;
    
    break_point on ;
    ;;
  
  3) # Step 3: Binding public key to your account in dashboard at https://dash.nodr.app
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Binding public key to your account in dashboard.${def}"  

    echo -e "${inf}Your public key:${def}"
    echo -e "${def}`cat ${KEY_PATH}/${KEY_PUBLIC}`${def}"
    [ -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - public key use: [ `cat ${KEY_PATH}/${KEY_PUBLIC}` ]" >> ${LOG_FILE}

    case "`check_public_key`" in
      "true") 
        [ ${DEBUG_MODE} ] && echo -e "${inf}Congratulations! You've successfully bound your public key to your NODR dashboard!\n Shall we continue?${def}"
        
        STEP=`echo ${STEP} + 1 | bc` ;
        ;;
        
      "false")
        reg_public_key ;
        
        [ ${STEP} -lt 100 ] && STEP=`echo ${STEP} + 1 | bc` ;
        ;;
        
      *) echo -e "${inf}${MSG_NODR_BACKEND_NOACCESS}${def}" ;;
    
    esac
    
    [ ${STEP} -lt 100 ] && break_point on ;
    ;;

  4) # Step 4. Collection HW configuration
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Collecting hardware information${def}"
    [ ${DEBUG_MODE} ] && echo -n -e "${inf} getting CPU info ... ${def}" ; 
    get_cpu_info ; 
    [ ${DEBUG_MODE} ] && echo -e "${inf}done${def}" ;
    [ ${DEBUG_MODE} ] && echo -n -e "${inf} getting RAM info ... ${def}" ; 
    get_ram_info ; 
    [ ${DEBUG_MODE} ] && echo -e "${inf}done${def}" ;
    
    [ ${DEBUG_MODE} ] && echo -e "${inf}The hardware information has been obtained successfully.\n Shall we continue?${def}"  

    STEP=`echo ${STEP} + 1 | bc` ;
    [ ${STEP} -lt 100 ] && break_point on ;
    ;;
    
  5) # Step 5. Config network
    _STEP=${STEP} ;
    NEXT_STEP=`echo ${STEP} + 1 | bc` ;
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Configuring network getting IP addresses of the server.${def}" ;
    set_inet ;
    
    while [ ${STEP} -eq ${_STEP} -a ${NEXT_STEP} -ne ${_STEP} ] 
    do 

      # listen NODR_USE_IP:NODR_PORT for 1 web connect (1 - log, backend) + 4 reserve 
      nohup echo -e "HTTP/1.1 200 OK\n" 2>/dev/null | ${NCAT} -l ${NODR_RUN_IP} ${NODR_PORT} 1>/dev/null 2>&1 && \
            echo -e "HTTP/1.1 200 OK\n" 2>/dev/null | ${NCAT} -l ${NODR_RUN_IP} ${NODR_PORT} 1>/dev/null 2>&1 && \
            echo -e "HTTP/1.1 200 OK\n" 2>/dev/null | ${NCAT} -l ${NODR_RUN_IP} ${NODR_PORT} 1>/dev/null 2>&1 && \
            echo -e "HTTP/1.1 200 OK\n" 2>/dev/null | ${NCAT} -l ${NODR_RUN_IP} ${NODR_PORT} 1>/dev/null 2>&1 && \
            echo -e "HTTP/1.1 200 OK\n" 2>/dev/null | ${NCAT} -l ${NODR_RUN_IP} ${NODR_PORT} 1>/dev/null 2>&1 &
    
      sleep 1 ;
      
      case "`check_nodr_ip`" in
      
        "true")
          echo -e "${inf}Congratulations! Nodr available from inet on ${NODR_IP}:${NODR_PORT}.${def}" ;
          
          [ ${DEBUG_MODE} ] && echo -n -e "${inf} getting bandwidth info ... ${def}" ; 
          get_bandwidth_info ;
          
          BANDWIDTH_UPLOAD_LIMIT=${BANDWIDTH_UPLOAD} ;
          [ ${DEBUG_MODE} ] && echo -e "${inf}done${def}" ;
          
          echo -e "${inf}Measured bandwidth is \"`bandwidth_human ${BANDWIDTH_UPLOAD}`\" Mbit/s${def}" ;
          echo -e "${inf} Shall NODR use all of it, or you'd like to limit the bandwidth?${def}" ;
          
          while [ 1 -eq 1 ]
          do
          
            echo -e "${def}1 - O${act}k, let NODR use all of it, or${def} 2 - N${act}o, let's limit the bandwidth${def}" ;
            echo -en "${act}Your choice [${key} 1 ${act}]: ${def}" ; 

            read ANSWER
    
            case ${ANSWER} in
            
              1|"O"|"o"|"") 
                echo -e "${inf}No limit used.${def}" ;
                ;;
              
              2|"N"|"n")
                while [ 1 -eq 1 ]
                do
                
                  echo -e -n "\n${inf} Limit bandwidth Mbit/s [ `bandwidth_human ${BANDWIDTH_UPLOAD_LIMIT}` ]: ${def}"
                  read ANSWER
                
                  case ${ANSWER} in
                  
                    '') 
                      break ;
                      ;;
                    
                    [0-9]*)
                      # soft_limit
                      [ ${ANSWER} -gt 0 -a ${ANSWER} -le `bandwidth_human ${BANDWIDTH_UPLOAD_MAX_LIMIT}` ] && BANDWIDTH_UPLOAD_LIMIT=`bandwidth_system ${ANSWER}` && break || \
                      echo -e "${inf}Incorrect bandwidth value is entered.${def}" ;
                      ;;
                    
                    *) 
                      echo -e "${inf}Incorrect limit bandwidth value is entered.${def}" ;
                      ;;
                      
                  esac
                  
                done 
                ;;
              
              *) 
                echo -e "${inf}Incorrect value is entered.${def}" ;
                continue ;
                ;;
                
            esac ;
            
            break ;
            
          done ;
        
          [ ${DEBUG_MODE} ] && echo -e "\n${inf}The network has been configured successfully.\n Shall we continue?${def}" ;
      
          STEP=`echo ${STEP} + 1 | bc` ;
          ;;
        
        "false")
          echo -e "${inf}Unfortunately, we were unable to reach you server on ${NODR_IP}:${NODR_PORT}. The NODR app won't work with unreachable ${NODR_IP}:${NODR_PORT}.\n Shall we continue?${def}" ;
          break_point ynr_check_nodr_ip ;
          ;;
              
        *) echo -e "${inf}${MSG_NODR_BACKEND_NOACCESS}${def}" ;;
                
      esac

      # free NODR_PORT for reserv 10
      for i in {1..10}
      do
        ${CURL} ${NODR_RUN_IP}:${NODR_PORT} 1>/dev/null 2>&1 ;
      done
      
    done
    
    # reconfigure - not breackpoint
    [ ${NEXT_STEP} -eq ${_STEP} ] && continue ;
    
    [ ${STEP} -lt 100 ] && break_point onr ;
    ;;
    
  6) # Step 6. Storage configuration.
    _STEP=${STEP} ;
    NEXT_STEP=`echo ${STEP} + 1 | bc` ;
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Specifying the storage location${def}" ;
    ##set_storage_dir ;
    
    set_storage_multistore ;
    
    [ ${NODR_MULTISTORE_SIZE} -le 0 ] && continue ;
    [ ${DEBUG_MODE} ] && echo -e "${inf} Shall we continue?${def}"
    STEP=`echo ${STEP} + 1 | bc` ;
    
    [ ${STEP} -lt 100 ] && break_point ynr_set_storage ;

    # reconfigure
    [ ${NEXT_STEP} -eq ${_STEP} ] && NODR_STORAGE_DIR="" && NODR_STORAGE_DRIVE="" ;
    ;;

  7) # Step 7. Sending HW configuration to backend, get a successful result & Docker-compose || ( get bad result && exit )
    _STEP=${STEP} ;
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Matching your hardware configuration to required minimum ${def}" ;

    while [ ${STEP} -eq ${_STEP} ] 
    do 
    
      # API - chk created NODR
      CHECK_CONFIG_NODR="`check_config_nodr`"
      [ ${DEBUG_MODE} ] && echo -e ${CHECK_CONFIG_NODR} 1>>${LOG_FILE} 2>&1 ;
     
      # info compare aim && used parameters
      echo -e "${inf}The results of the configuration check: ${def}" ;
      info_check_config_nodr os ;
      info_check_config_nodr cpu_frequency ;
      info_check_config_nodr ram ;
      info_check_config_nodr bandwidth ;
      info_check_config_nodr storage ;
      #info_check_config_nodr ip ;
      #info_check_config_nodr port ;      
      
      case "`echo ${CHECK_CONFIG_NODR} | jq '.result'`" in
      
        "true") 
          echo -e "${inf}Congratulations! Your server is fully compatible with NODR application and is ready for installation.${def}"
          echo -e "${inf} Shall we install the NODR application?${def}" ;
          [ ${DEBUG_MODE} -a -n "${LOG_FILE}" ] && echo -e "$DATE - Step ${STEP}. OK - Configuration NODR [ Congratulations! Your server is fully compatible with NODR application and is ready for installation ]" >> ${LOG_FILE} ;
          STEP=`echo ${STEP} + 1 | bc`  ;
          
          save_config_nodr ;
          
          break_point yn_install  ;
          continue ;
          ;;
      
        "false") 
          echo -e "${inf}Unfortunately, the configuration check have failed. Please upgrade your server to fir the minimum requirements. In case is you need support please contact us via support@nodr.app${def}" ;
          echo -e "${err}The installation procedure is terminated.${def}" ; 
          echo -e "${info} Run check again?" ;
          break_point onr_check_config ;
          
          break ;
          ;;
      
        *) 
          echo -e "${inf}${MSG_NODR_BACKEND_NOACCESS}${def}" ;
          echo -e "${err}The installation procedure is terminated${def}" ; 
          STEP=`echo ${STEP} + 100 | bc`  ;
          break ;
          ;;
          
      esac

    done

    ;;
    
  8) # Step 8. NODR app automated installation. NODR run.

## ---------------- multiOS_04 -------------- ##
    [ ${DEBUG_MODE} ] && echo -e "${inf}Step ${STEP}. Install && run NODR ${def}" ;
    case "`os_id`" in
    "almalinux")
      update_system_almalinux ;
      install_pkg_almalinux ;
      ;;

    "centos")
      update_system_centos ;
      install_pkg_centos ;
      ;;

    "debian")
      update_system_debian ;
      install_pkg_debian ;
      ;;

    "ubuntu")
      update_system_ubuntu ;
      install_pkg_ubuntu ;
      ;;

    esac
## --------------- _multiOS_04 -------------- ##

    check_docker_installed ;
    configure_env_nodr ;
    install_nodr ;
    run_nodr && echo -e "${inf}NODR successfully launched.${def}" && sleep 5 ||
                echo -e "${err}NODR unsuccessfully launched.${def}"; 

    # set_active_nodr
    NEXT_STEP=${STEP}
    while [ ${NEXT_STEP} -eq ${STEP} ] 
    do 
    
      case "`set_active_nodr`" in
      
        "true")
          [ ${DEBUG_MODE} ] && echo -e "${inf}NODR successfully activated.${def}" ;
          STEP=`echo ${STEP} + 1 | bc` ;
          ;;
        
        "false")
          echo -e "${inf}NODR unsuccessfully activated${def} on ${NODR_IP}:${NODR_PORT}" ;
        
          [ ${STEP} -lt 100 ] && break_point ynr_set_active ;
          
          continue ;
          ;;
              
        *) 
          echo -e "${inf}${MSG_NODR_BACKEND_NOACCESS}${def}" ;
          
          [ ${STEP} -lt 100 ] && break_point yn  ;
          ;;
                  
      esac
      
    done
    ;;
    
  *) 
    echo "Unconfigured ${STEP} step !!!" && exit 1 ;
    ;;

esac

done